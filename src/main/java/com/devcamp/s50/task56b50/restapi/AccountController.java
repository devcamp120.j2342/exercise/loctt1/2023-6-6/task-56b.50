package com.devcamp.s50.task56b50.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {
     @GetMapping("/accounts")
     public ArrayList<Account> getAccounts(){
          //khởi tạo danh sách
          ArrayList<Account> accounts = new ArrayList<>();

          //tạo đối tượng khách hàng
        Customer customer1 = new Customer(1, "Tan Loc", 20000);
        Customer customer2 = new Customer(2, "My Cam", 30000);
        Customer customer3 = new Customer(3, "Phat Tai", 40000);

         // In thông tin khách hàng ra console
         System.out.println(customer1.toString());
         System.out.println(customer2.toString());
         System.out.println(customer3.toString());
 
          // Tạo các đối tượng tài khoản tương ứng với khách hàng
          Account accounts1 = new Account(1, customer1, 10000.0);
          Account accounts2 = new Account(2, customer2, 20000.0);
          Account accounts3 = new Account(3, customer3, 30000.0);

            // In thông tin tài khoản ra console
          System.out.println(accounts1.toString());
          System.out.println(accounts2.toString());
          System.out.println(accounts3.toString());

          // Thêm các đối tượng tài khoản vào danh sách
          accounts.add(accounts1);
          accounts.add(accounts2);
          accounts.add(accounts3);

          return accounts;
     }
}
